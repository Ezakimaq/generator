package com.temple.generator.service;

import org.springframework.http.ResponseEntity;

public interface WeatherService {

    String getRandomWeather(String season);

    ResponseEntity<?> addWeatherToSeason(String season);
}
