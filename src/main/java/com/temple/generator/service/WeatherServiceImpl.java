package com.temple.generator.service;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class WeatherServiceImpl implements WeatherService {

    @Override
    public String getRandomWeather(String season) {
        String[] summer = new String[]{"Heavy Winds", "Very Hot", "Hot Clear", "Warm", "Clear", "Warm with Cool Breeze"};
        return summer[createRandomInteger()];
    }

    @Override
    public ResponseEntity<?> addWeatherToSeason(String season) {
        return null;
    }

    private int createRandomInteger(){
        var random = new Random();
        return random.nextInt(6);
    }
}
