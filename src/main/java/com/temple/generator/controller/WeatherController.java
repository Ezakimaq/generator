package com.temple.generator.controller;

import com.temple.generator.service.WeatherService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1")
@CrossOrigin(origins = "http://localhost:8080")
public class WeatherController {
    private final WeatherService weatherService;

    @GetMapping(value = "/weather/{season}")
    public String getRandomWeather(@PathVariable String season) {
        return weatherService.getRandomWeather(season);
    }

    @PostMapping(value = "/weather/{season}")
    public ResponseEntity<?> addWeather(@PathVariable String season) {
        return weatherService.addWeatherToSeason(season);
    }
}
