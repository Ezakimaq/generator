package com.temple.generator.model;

import java.util.List;

public record Weather(List<String> summer, List<String> fall, List<String> winter, List<String> spring) {
}
