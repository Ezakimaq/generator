package com.temple.generator.model;

import java.util.List;

public record NPC(String name, String purpose, List<String> traits, String looks) {

    public boolean isQuestGiver(){
        return false;
    }
}
