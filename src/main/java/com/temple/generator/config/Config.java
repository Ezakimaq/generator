package com.temple.generator.config;

import com.temple.generator.service.WeatherService;
import com.temple.generator.service.WeatherServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class Config {

    @Bean
    public WeatherService weatherService() {
        return new WeatherServiceImpl();
    }
}
