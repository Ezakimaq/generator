package com.temple.generator.repository;

import com.temple.generator.model.Weather;

public interface WeatherDao {

    Weather getRandomWeather(String season);
}
